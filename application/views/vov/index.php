<div class="pull-right">
	<a href="<?php echo site_url('vov/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Vov</th>
		<th>Id Perfomance</th>
		<th>Id Master Obat</th>
		<th>Id Kandang</th>
		<th>Id Periode</th>
		<th>Pemakaian</th>
		<th>Actions</th>
    </tr>
	<?php foreach($vov as $v){ ?>
    <tr>
		<td><?php echo $v['id_vov']; ?></td>
		<td><?php echo $v['id_perfomance']; ?></td>
		<td><?php echo $v['id_master_obat']; ?></td>
		<td><?php echo $v['id_kandang']; ?></td>
		<td><?php echo $v['id_periode']; ?></td>
		<td><?php echo $v['pemakaian']; ?></td>
		<td>
            <a href="<?php echo site_url('vov/edit/'.$v['id_vov']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('vov/remove/'.$v['id_vov']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
