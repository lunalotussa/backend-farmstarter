<?php echo form_open('pakan/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_perfomance" class="col-md-4 control-label">Id Perfomance</label>
		<div class="col-md-8">
			<input type="text" name="id_perfomance" value="<?php echo $this->input->post('id_perfomance'); ?>" class="form-control" id="id_perfomance" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_master_pakan" class="col-md-4 control-label">Id Master Pakan</label>
		<div class="col-md-8">
			<input type="text" name="id_master_pakan" value="<?php echo $this->input->post('id_master_pakan'); ?>" class="form-control" id="id_master_pakan" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_kandang" class="col-md-4 control-label">Id Kandang</label>
		<div class="col-md-8">
			<input type="text" name="id_kandang" value="<?php echo $this->input->post('id_kandang'); ?>" class="form-control" id="id_kandang" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_periode" class="col-md-4 control-label">Id Periode</label>
		<div class="col-md-8">
			<input type="text" name="id_periode" value="<?php echo $this->input->post('id_periode'); ?>" class="form-control" id="id_periode" />
		</div>
	</div>
	<div class="form-group">
		<label for="pemakaian" class="col-md-4 control-label">Pemakaian</label>
		<div class="col-md-8">
			<input type="text" name="pemakaian" value="<?php echo $this->input->post('pemakaian'); ?>" class="form-control" id="pemakaian" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>