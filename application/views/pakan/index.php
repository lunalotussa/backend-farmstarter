<div class="pull-right">
	<a href="<?php echo site_url('pakan/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Pakan</th>
		<th>Id Perfomance</th>
		<th>Id Master Pakan</th>
		<th>Id Kandang</th>
		<th>Id Periode</th>
		<th>Pemakaian</th>
		<th>Actions</th>
    </tr>
	<?php foreach($pakan as $p){ ?>
    <tr>
		<td><?php echo $p['id_pakan']; ?></td>
		<td><?php echo $p['id_perfomance']; ?></td>
		<td><?php echo $p['id_master_pakan']; ?></td>
		<td><?php echo $p['id_kandang']; ?></td>
		<td><?php echo $p['id_periode']; ?></td>
		<td><?php echo $p['pemakaian']; ?></td>
		<td>
            <a href="<?php echo site_url('pakan/edit/'.$p['id_pakan']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('pakan/remove/'.$p['id_pakan']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
