<div class="pull-right">
	<a href="<?php echo site_url('kandang/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Kandang</th>
		<th>Nama</th>
		<th>Actions</th>
    </tr>
	<?php foreach($kandang as $k){ ?>
    <tr>
		<td><?php echo $k['id_kandang']; ?></td>
		<td><?php echo $k['nama']; ?></td>
		<td>
            <a href="<?php echo site_url('kandang/edit/'.$k['id_kandang']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('kandang/remove/'.$k['id_kandang']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
