<div class="pull-right">
	<a href="<?php echo site_url('master_vov/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Master Vov</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>Indikasi</th>
		<th>Komposisi</th>
		<th>Aturan Pakai</th>
		<th>Minimum Stock</th>
		<th>Stock</th>
		<th>Actions</th>
    </tr>
	<?php foreach($master_vov as $m){ ?>
    <tr>
		<td><?php echo $m['id_master_vov']; ?></td>
		<td><?php echo $m['kode']; ?></td>
		<td><?php echo $m['nama']; ?></td>
		<td><?php echo $m['indikasi']; ?></td>
		<td><?php echo $m['komposisi']; ?></td>
		<td><?php echo $m['aturan_pakai']; ?></td>
		<td><?php echo $m['minimum_stock']; ?></td>
		<td><?php echo $m['stock']; ?></td>
		<td>
            <a href="<?php echo site_url('master_vov/edit/'.$m['id_master_vov']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('master_vov/remove/'.$m['id_master_vov']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
