<?php echo form_open('master_vov/edit/'.$master_vov['id_master_vov'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="kode" class="col-md-4 control-label">Kode</label>
		<div class="col-md-8">
			<input type="text" name="kode" value="<?php echo ($this->input->post('kode') ? $this->input->post('kode') : $master_vov['kode']); ?>" class="form-control" id="kode" />
		</div>
	</div>
	<div class="form-group">
		<label for="nama" class="col-md-4 control-label">Nama</label>
		<div class="col-md-8">
			<input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $master_vov['nama']); ?>" class="form-control" id="nama" />
		</div>
	</div>
	<div class="form-group">
		<label for="indikasi" class="col-md-4 control-label">Indikasi</label>
		<div class="col-md-8">
			<input type="text" name="indikasi" value="<?php echo ($this->input->post('indikasi') ? $this->input->post('indikasi') : $master_vov['indikasi']); ?>" class="form-control" id="indikasi" />
		</div>
	</div>
	<div class="form-group">
		<label for="komposisi" class="col-md-4 control-label">Komposisi</label>
		<div class="col-md-8">
			<input type="text" name="komposisi" value="<?php echo ($this->input->post('komposisi') ? $this->input->post('komposisi') : $master_vov['komposisi']); ?>" class="form-control" id="komposisi" />
		</div>
	</div>
	<div class="form-group">
		<label for="aturan_pakai" class="col-md-4 control-label">Aturan Pakai</label>
		<div class="col-md-8">
			<input type="text" name="aturan_pakai" value="<?php echo ($this->input->post('aturan_pakai') ? $this->input->post('aturan_pakai') : $master_vov['aturan_pakai']); ?>" class="form-control" id="aturan_pakai" />
		</div>
	</div>
	<div class="form-group">
		<label for="minimum_stock" class="col-md-4 control-label">Minimum Stock</label>
		<div class="col-md-8">
			<input type="text" name="minimum_stock" value="<?php echo ($this->input->post('minimum_stock') ? $this->input->post('minimum_stock') : $master_vov['minimum_stock']); ?>" class="form-control" id="minimum_stock" />
		</div>
	</div>
	<div class="form-group">
		<label for="stock" class="col-md-4 control-label">Stock</label>
		<div class="col-md-8">
			<input type="text" name="stock" value="<?php echo ($this->input->post('stock') ? $this->input->post('stock') : $master_vov['stock']); ?>" class="form-control" id="stock" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>