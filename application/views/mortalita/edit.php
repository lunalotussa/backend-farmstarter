<?php echo form_open('mortalita/edit/'.$mortalita['id_mortalitas'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_perfomance" class="col-md-4 control-label">Id Perfomance</label>
		<div class="col-md-8">
			<input type="text" name="id_perfomance" value="<?php echo ($this->input->post('id_perfomance') ? $this->input->post('id_perfomance') : $mortalita['id_perfomance']); ?>" class="form-control" id="id_perfomance" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_kandang" class="col-md-4 control-label">Id Kandang</label>
		<div class="col-md-8">
			<input type="text" name="id_kandang" value="<?php echo ($this->input->post('id_kandang') ? $this->input->post('id_kandang') : $mortalita['id_kandang']); ?>" class="form-control" id="id_kandang" />
		</div>
	</div>
	<div class="form-group">
		<label for="id_periode" class="col-md-4 control-label">Id Periode</label>
		<div class="col-md-8">
			<input type="text" name="id_periode" value="<?php echo ($this->input->post('id_periode') ? $this->input->post('id_periode') : $mortalita['id_periode']); ?>" class="form-control" id="id_periode" />
		</div>
	</div>
	<div class="form-group">
		<label for="unknown" class="col-md-4 control-label">Unknown</label>
		<div class="col-md-8">
			<input type="text" name="unknown" value="<?php echo ($this->input->post('unknown') ? $this->input->post('unknown') : $mortalita['unknown']); ?>" class="form-control" id="unknown" />
		</div>
	</div>
	<div class="form-group">
		<label for="prolab" class="col-md-4 control-label">Prolab</label>
		<div class="col-md-8">
			<input type="text" name="prolab" value="<?php echo ($this->input->post('prolab') ? $this->input->post('prolab') : $mortalita['prolab']); ?>" class="form-control" id="prolab" />
		</div>
	</div>
	<div class="form-group">
		<label for="lumpuh" class="col-md-4 control-label">Lumpuh</label>
		<div class="col-md-8">
			<input type="text" name="lumpuh" value="<?php echo ($this->input->post('lumpuh') ? $this->input->post('lumpuh') : $mortalita['lumpuh']); ?>" class="form-control" id="lumpuh" />
		</div>
	</div>
	<div class="form-group">
		<label for="sakit" class="col-md-4 control-label">Sakit</label>
		<div class="col-md-8">
			<input type="text" name="sakit" value="<?php echo ($this->input->post('sakit') ? $this->input->post('sakit') : $mortalita['sakit']); ?>" class="form-control" id="sakit" />
		</div>
	</div>
	<div class="form-group">
		<label for="total_mortalitas" class="col-md-4 control-label">Total Mortalitas</label>
		<div class="col-md-8">
			<input type="text" name="total_mortalitas" value="<?php echo ($this->input->post('total_mortalitas') ? $this->input->post('total_mortalitas') : $mortalita['total_mortalitas']); ?>" class="form-control" id="total_mortalitas" />
		</div>
	</div>
	<div class="form-group">
		<label for="total_akhir" class="col-md-4 control-label">Total Akhir</label>
		<div class="col-md-8">
			<input type="text" name="total_akhir" value="<?php echo ($this->input->post('total_akhir') ? $this->input->post('total_akhir') : $mortalita['total_akhir']); ?>" class="form-control" id="total_akhir" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>