<div class="pull-right">
	<a href="<?php echo site_url('mortalita/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Mortalitas</th>
		<th>Id Perfomance</th>
		<th>Id Kandang</th>
		<th>Id Periode</th>
		<th>Unknown</th>
		<th>Prolab</th>
		<th>Lumpuh</th>
		<th>Sakit</th>
		<th>Total Mortalitas</th>
		<th>Total Akhir</th>
		<th>Actions</th>
    </tr>
	<?php foreach($mortalitas as $m){ ?>
    <tr>
		<td><?php echo $m['id_mortalitas']; ?></td>
		<td><?php echo $m['id_perfomance']; ?></td>
		<td><?php echo $m['id_kandang']; ?></td>
		<td><?php echo $m['id_periode']; ?></td>
		<td><?php echo $m['unknown']; ?></td>
		<td><?php echo $m['prolab']; ?></td>
		<td><?php echo $m['lumpuh']; ?></td>
		<td><?php echo $m['sakit']; ?></td>
		<td><?php echo $m['total_mortalitas']; ?></td>
		<td><?php echo $m['total_akhir']; ?></td>
		<td>
            <a href="<?php echo site_url('mortalita/edit/'.$m['id_mortalitas']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('mortalita/remove/'.$m['id_mortalitas']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
