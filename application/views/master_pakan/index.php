<div class="pull-right">
	<a href="<?php echo site_url('master_pakan/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Master Pakan</th>
		<th>Kode</th>
		<th>Nama</th>
		<th>Presentase Nutrisi</th>
		<th>Minimum Stock</th>
		<th>Stock</th>
		<th>Actions</th>
    </tr>
	<?php foreach($master_pakan as $m){ ?>
    <tr>
		<td><?php echo $m['id_master_pakan']; ?></td>
		<td><?php echo $m['kode']; ?></td>
		<td><?php echo $m['nama']; ?></td>
		<td><?php echo $m['presentase_nutrisi']; ?></td>
		<td><?php echo $m['minimum_stock']; ?></td>
		<td><?php echo $m['stock']; ?></td>
		<td>
            <a href="<?php echo site_url('master_pakan/edit/'.$m['id_master_pakan']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('master_pakan/remove/'.$m['id_master_pakan']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
