<?php echo form_open('master_pakan/edit/'.$master_pakan['id_master_pakan'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="kode" class="col-md-4 control-label">Kode</label>
		<div class="col-md-8">
			<input type="text" name="kode" value="<?php echo ($this->input->post('kode') ? $this->input->post('kode') : $master_pakan['kode']); ?>" class="form-control" id="kode" />
		</div>
	</div>
	<div class="form-group">
		<label for="nama" class="col-md-4 control-label">Nama</label>
		<div class="col-md-8">
			<input type="text" name="nama" value="<?php echo ($this->input->post('nama') ? $this->input->post('nama') : $master_pakan['nama']); ?>" class="form-control" id="nama" />
		</div>
	</div>
	<div class="form-group">
		<label for="presentase_nutrisi" class="col-md-4 control-label">Presentase Nutrisi</label>
		<div class="col-md-8">
			<input type="text" name="presentase_nutrisi" value="<?php echo ($this->input->post('presentase_nutrisi') ? $this->input->post('presentase_nutrisi') : $master_pakan['presentase_nutrisi']); ?>" class="form-control" id="presentase_nutrisi" />
		</div>
	</div>
	<div class="form-group">
		<label for="minimum_stock" class="col-md-4 control-label">Minimum Stock</label>
		<div class="col-md-8">
			<input type="text" name="minimum_stock" value="<?php echo ($this->input->post('minimum_stock') ? $this->input->post('minimum_stock') : $master_pakan['minimum_stock']); ?>" class="form-control" id="minimum_stock" />
		</div>
	</div>
	<div class="form-group">
		<label for="stock" class="col-md-4 control-label">Stock</label>
		<div class="col-md-8">
			<input type="text" name="stock" value="<?php echo ($this->input->post('stock') ? $this->input->post('stock') : $master_pakan['stock']); ?>" class="form-control" id="stock" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>