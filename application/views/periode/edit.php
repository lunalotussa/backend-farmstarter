<?php echo form_open('periode/edit/'.$periode['id_periode'],array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_kandang" class="col-md-4 control-label">Id Kandang</label>
		<div class="col-md-8">
			<input type="text" name="id_kandang" value="<?php echo ($this->input->post('id_kandang') ? $this->input->post('id_kandang') : $periode['id_kandang']); ?>" class="form-control" id="id_kandang" />
		</div>
	</div>
	<div class="form-group">
		<label for="name" class="col-md-4 control-label">Name</label>
		<div class="col-md-8">
			<input type="text" name="name" value="<?php echo ($this->input->post('name') ? $this->input->post('name') : $periode['name']); ?>" class="form-control" id="name" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>
	
<?php echo form_close(); ?>