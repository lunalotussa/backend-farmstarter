<div class="pull-right">
	<a href="<?php echo site_url('periode/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Periode</th>
		<th>Id Kandang</th>
		<th>Name</th>
		<th>Actions</th>
    </tr>
	<?php foreach($periode as $p){ ?>
    <tr>
		<td><?php echo $p['id_periode']; ?></td>
		<td><?php echo $p['id_kandang']; ?></td>
		<td><?php echo $p['name']; ?></td>
		<td>
            <a href="<?php echo site_url('periode/edit/'.$p['id_periode']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('periode/remove/'.$p['id_periode']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
