<div class="pull-right">
	<a href="<?php echo site_url('perfomance/add'); ?>" class="btn btn-success">Add</a> 
</div>

<table class="table table-striped table-bordered">
    <tr>
		<th>Id Perfomance</th>
		<th>Id Periode</th>
		<th>Tanggal</th>
		<th>Actions</th>
    </tr>
	<?php foreach($perfomance as $p){ ?>
    <tr>
		<td><?php echo $p['id_perfomance']; ?></td>
		<td><?php echo $p['id_periode']; ?></td>
		<td><?php echo $p['tanggal']; ?></td>
		<td>
            <a href="<?php echo site_url('perfomance/edit/'.$p['id_perfomance']); ?>" class="btn btn-info btn-xs">Edit</a> 
            <a href="<?php echo site_url('perfomance/remove/'.$p['id_perfomance']); ?>" class="btn btn-danger btn-xs">Delete</a>
        </td>
    </tr>
	<?php } ?>
</table>
