<?php echo form_open('perfomance/add',array("class"=>"form-horizontal")); ?>

	<div class="form-group">
		<label for="id_periode" class="col-md-4 control-label">Id Periode</label>
		<div class="col-md-8">
			<input type="text" name="id_periode" value="<?php echo $this->input->post('id_periode'); ?>" class="form-control" id="id_periode" />
		</div>
	</div>
	<div class="form-group">
		<label for="tanggal" class="col-md-4 control-label">Tanggal</label>
		<div class="col-md-8">
			<input type="text" name="tanggal" value="<?php echo $this->input->post('tanggal'); ?>" class="form-control" id="tanggal" />
		</div>
	</div>
	
	<div class="form-group">
		<div class="col-sm-offset-4 col-sm-8">
			<button type="submit" class="btn btn-success">Save</button>
        </div>
	</div>

<?php echo form_close(); ?>