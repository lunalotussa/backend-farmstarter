<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Perfomance_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get perfomance by id_perfomance
     */
    function get_perfomance($id_perfomance)
    {
        return $this->db->get_where('perfomance',array('id_perfomance'=>$id_perfomance))->row_array();
    }
        
    /*
     * Get all perfomance
     */
    function get_all_perfomance()
    {
        $this->db->order_by('id_perfomance', 'desc');
        return $this->db->get('perfomance')->result_array();
    }
        
    /*
     * function to add new perfomance
     */
    function add_perfomance($params)
    {
        $this->db->insert('perfomance',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update perfomance
     */
    function update_perfomance($id_perfomance,$params)
    {
        $this->db->where('id_perfomance',$id_perfomance);
        return $this->db->update('perfomance',$params);
    }
    
    /*
     * function to delete perfomance
     */
    function delete_perfomance($id_perfomance)
    {
        return $this->db->delete('perfomance',array('id_perfomance'=>$id_perfomance));
    }
}
