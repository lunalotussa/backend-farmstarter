<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Vov_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get vov by id_vov
     */
    function get_vov($id_vov)
    {
        return $this->db->get_where('vov',array('id_vov'=>$id_vov))->row_array();
    }
        
    /*
     * Get all vov
     */
    function get_all_vov()
    {
        $this->db->order_by('id_vov', 'desc');
        return $this->db->get('vov')->result_array();
    }
        
    /*
     * function to add new vov
     */
    function add_vov($params)
    {
        $this->db->insert('vov',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update vov
     */
    function update_vov($id_vov,$params)
    {
        $this->db->where('id_vov',$id_vov);
        return $this->db->update('vov',$params);
    }
    
    /*
     * function to delete vov
     */
    function delete_vov($id_vov)
    {
        return $this->db->delete('vov',array('id_vov'=>$id_vov));
    }
}
