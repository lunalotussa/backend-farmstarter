<?php
/* 
 * Generated by CRUDigniter v3.2 
 * www.crudigniter.com
 */
 
class Master_vov_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    /*
     * Get master_vov by id_master_vov
     */
    function get_master_vov($id_master_vov)
    {
        return $this->db->get_where('master_vov',array('id_master_vov'=>$id_master_vov))->row_array();
    }
        
    /*
     * Get all master_vov
     */
    function get_all_master_vov()
    {
        $this->db->order_by('id_master_vov', 'desc');
        return $this->db->get('master_vov')->result_array();
    }
        
    /*
     * function to add new master_vov
     */
    function add_master_vov($params)
    {
        $this->db->insert('master_vov',$params);
        return $this->db->insert_id();
    }
    
    /*
     * function to update master_vov
     */
    function update_master_vov($id_master_vov,$params)
    {
        $this->db->where('id_master_vov',$id_master_vov);
        return $this->db->update('master_vov',$params);
    }
    
    /*
     * function to delete master_vov
     */
    function delete_master_vov($id_master_vov)
    {
        return $this->db->delete('master_vov',array('id_master_vov'=>$id_master_vov));
    }
}
